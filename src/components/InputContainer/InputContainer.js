import React,{Component} from 'react';

class InputContainer extends Component{

    constructor(props){
        super(props);

        this.onClickAddTask = this.onClickAddTask.bind(this);
    }

    onClickAddTask(){
       
        if(this.refs.addTaskInput.value.length > 0){
            let n = Date();
            n = JSON.stringify(n);
            if(this.refs.addTaskDetail.value.length < 1){
                this.refs.addTaskDetail.value = "NONE";
            }
            this.refs.addTaskDetail.value = "Detail of the task : "+this.refs.addTaskDetail.value
            n = "Time when added : "+n;

            this.props.inTaskAdd(this.refs.addTaskInput.value,this.refs.addTaskDetail.value,n);

            this.refs.addTaskInput.value = "";
            this.refs.addTaskDetail.value = "";
        }
        else{
            alert("No task add \n You must add least one character at new task");
        }
        
    }

    render(){
        return(
            <div>
                <h5>To Do List</h5>
                <input ref="addTaskInput" placeholder="Add new task..." className="taskInput"/>
                <h5>Detail</h5>
                <input ref="addTaskDetail" placeholder="Add Detail..." className="taskInput"/>
                <br/> 
                <button className="btn-add" onClick={this.onClickAddTask}>ADD</button>
                <br/>
                <iframe className="iron-man" src="https://www.youtube.com/embed/jNo3zmhXE9Y?autoplay=1"></iframe>
                
            </div>
        );
    }
}

export default InputContainer;