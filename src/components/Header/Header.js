import React from 'react'

const Header = ()=>{
    return <div className="Header">
    		<h1>EASY LIFE</h1>
    		<h2>Manage your life for better life</h2>
    	</div>
};

export default Header;