import React, {Component} from 'react';
import Header from './Header/Header';
import InputContainer from './InputContainer/InputContainer';
import TodoList from './TodoList/TodoList';

class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            task: '',
            timeOfTask: '',
            detail: ''
        };

        this.onTaskAdd = this.onTaskAdd.bind(this);
    }

    onTaskAdd(value,detail,time){
        this.setState({task: value});
        this.setState({timeOfTask: time});
        this.setState({detail: detail});

    }

    render() {
        return (
            <div >
                <Header/>
                <div className="main-web" >
                    <div className="Input-Container"><InputContainer inTaskAdd={(value,detail,time)=>{this.onTaskAdd(value,detail,time)}}/></div>
                    <div className="Todo-List"> 
                        <TodoList timeOfTask={this.state.timeOfTask} 
                        task={this.state.task} 
                        detail={this.state.detail}/>
                    </div>                   
                </div>
                
            </div>
        );
    }
}

export default App;