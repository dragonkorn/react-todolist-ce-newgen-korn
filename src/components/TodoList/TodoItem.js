import React,{Component} from 'react';

class TodoItem extends Component{
    constructor(props){
        super(props);

        this.state = {
            Item : '',
            todoTime : ''
        };

        this.onClickDeleteTask = this.onClickDeleteTask.bind(this);
    }
    
    onClickDeleteTask(){
       
        this.props.onTaskDelete(this.props.task,this.props.timeOfTask,this.props.detail); 

    }

    render(){
        return(
            <div>
                <div>{this.props.task}</div>
                <div>{this.props.timeOfTask}</div>
                <div>{this.props.detail}</div>
                <button onClick={this.onClickDeleteTask}>delete</button>
            </div>
        );
    }
}

export default TodoItem;