import React,{Component} from 'react';

import TodoItem from './TodoItem';

class TodoList extends Component{

    constructor(props){
        super(props);
        
        this.state = {
            todoList: [],
            todoTime: [],
            todoDetail: []
        };

        this.renderListItem = this.renderListItem.bind(this);
    }

    onTaskDelete(value,time,detail){
        let todoList = this.state.todoList;
        let todoTime = this.state.todoTime;
        let todoDetail = this.state.todoDetail;
        let indexOfDelete;


        {
            let i = 0;
            for(i = 0;i<=todoList.length;i++){
                if(value === todoList[i] && time === todoTime[i] && detail === todoDetail[i]){
                    indexOfDelete = i;
                }
            }

        }

        if(indexOfDelete > -1){
            

            todoList.splice(indexOfDelete,1);
            todoTime.splice(indexOfDelete,1); 
            todoDetail.splice(indexOfDelete,1);


            this.setState({todoList,todoList});
            this.setState({todoTime,todoTime});
            this.setState({todoDetail,todoDetail});
        }
        

    }


    componentWillReceiveProps(nextProps) {
       
        let todoList = this.state.todoList;
        let todoTime = this.state.todoTime;
        let todoDetail = this.state.todoDetail;

    
        todoList.push(nextProps.task);
        this.setState({todoList,todoList});

        todoTime.push(nextProps.timeOfTask);
        this.setState({todoTime,todoTime});       
        
        todoDetail.push(nextProps.detail);
        this.setState({todoDetail,todoDetail});

        console.log("type : "+typeof(todoList)+ " length : "+todoList.length);
    }



    renderListItem(){
        return this.state.todoList.map((list,i)=>{
            return(
                <div className="todo-item">
                    <li key={i}>
                        <TodoItem 
                        onTaskDelete={(value,time,detail)=>{this.onTaskDelete(value,time,detail)}} 
                        task={list} 
                        timeOfTask={this.state.todoTime[i]} 
                        detail={this.state.todoDetail[i]}/>
                    </li>
                </div>    
            )
        })
    }
    
    render(){
        return(
            <div>
                <ul>
                    {this.renderListItem()}
                </ul>
            </div>        
        )
    }
}

export default TodoList;